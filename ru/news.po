msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-08-05 21:32-0700\n"
"PO-Revision-Date: 2024-09-25 14:27+0000\n"
"Last-Translator: Jenny Ryan <jenny@equalitie.org>\n"
"Language-Team: Russian <https://hosted.weblate.org/projects/censorship-no/"
"website/website-news/ru/>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Weblate 5.8-dev\n"

#: news.html%2Bhtml[lang]:4-1
msgid "en"
msgstr "ru"

#: news.html%2Bhtml.body.nav.div.div:96-7
msgctxt "news.html+html.body.nav.div.div:96-7"
msgid ""
"<a href=\"about.html\">About</a> <a href=\"support.html\">Support</a> <a "
"href=\"news.html\">News</a> <a href=\"help.html\">Community</a> <a "
"href=\"donate.html\">Donate</a>"
msgstr ""
"<a href=\"about.html\">О проекте</a><a href=\"support.html\">Поддержка</a><a "
"href=\"news.html\">Новости</a><a href=\"help.html\">Сообщество</a><a "
"href=\"donate.html\">Пожертвовать</a>"

#: news.html%2Bhtml.body.nav.div.div.div.label.img[alt]:107-13
msgctxt "news.html+html.body.nav.div.div.div.label.img[alt]:107-13"
msgid "Select language"
msgstr "Выбрать язык"

#: news.html%2Bhtml.body.nav.div.div.div.label.div:109-13
msgid "EN"
msgstr "RU"

#.  Sorted alphabetically by language code. 
#.  TRANSLATORS: Please do not translate any of these. 
#: news.html%2Bhtml.body.nav.div.div.div.div:112-11
#, fuzzy
msgctxt "news.html+html.body.nav.div.div.div.div:112-11"
msgid ""
"<a class=\"navbar-item\" lang=\"ar\" hreflang=\"ar\" href=\"../ar/news."
"html\">العربية</a> <a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" "
"href=\"../en/news.html\">English</a> <a class=\"navbar-item\" lang=\"es\" "
"hreflang=\"es\" href=\"../es/news.html\">Español</a> <a class=\"navbar-"
"item\" lang=\"fa\" hreflang=\"fa\" href=\"../fa/news.html\">فارسی</a> <a "
"class=\"navbar-item\" lang=\"fr\" hreflang=\"fr\" href=\"../fr/news."
"html\">Français</a> <a class=\"navbar-item\" lang=\"my\" hreflang=\"my\" "
"href=\"../my/news.html\">မြန်မာစာ</a> <a class=\"navbar-item\" lang=\"pt_BR\" "
"hreflang=\"pt_BR\" href=\"../pt_BR/news.html\">Português</a> <a "
"class=\"navbar-item\" lang=\"ru\" hreflang=\"ru\" href=\"../ru/news."
"html\">Pусский</a> <a class=\"navbar-item\" lang=\"tr\" hreflang=\"tr\" "
"href=\"../tr/news.html\">Türkçe</a> <a class=\"navbar-item\" lang=\"uk\" "
"hreflang=\"uk\" href=\"../uk/news.html\">Українська</a> <a class=\"navbar-"
"item\" lang=\"ur\" hreflang=\"ur\" href=\"../ur/news.html\">اردو</a>"
msgstr ""
"<a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" href=\"../en/news."
"html\">English</a> <a class=\"navbar-item\" lang=\"es\" hreflang=\"es\" "
"href=\"../es/news.html\">Español</a> <a class=\"navbar-item\" lang=\"fa\" "
"hreflang=\"fa\" href=\"../fa/news.html\">فارسی</a> <a class=\"navbar-item\" "
"lang=\"fr\" hreflang=\"fr\" href=\"../fr/news.html\">Français</a> <a "
"class=\"navbar-item\" lang=\"my\" hreflang=\"my\" href=\"../my/news."
"html\">မြန်မာစာ</a> <a class=\"navbar-item\" lang=\"ru\" hreflang=\"ru\" "
"href=\"../ru/news.html\">Pусский</a> <a class=\"navbar-item\" lang=\"uk\" "
"hreflang=\"uk\" href=\"../uk/news.html\">Українська</a>"

#: news.html%2Bhtml.body.nav.div.div:127-9
msgctxt "news.html+html.body.nav.div.div:127-9"
msgid "Download"
msgstr "Скачать"

#: news.html%2Bhtml.body.nav.div.div:138-7
msgctxt "news.html+html.body.nav.div.div:138-7"
msgid ""
"<a href=\"about.html\">About</a> <a href=\"support.html\">Support</a> <a "
"href=\"news.html\">News</a> <a href=\"help.html\">Community</a> <a "
"href=\"donate.html\">Donate</a>"
msgstr ""
"<a href=\"about.html\">О проекте</a><a href=\"support.html\">Поддержка</a><a "
"href=\"news.html\">Новости</a><a href=\"help.html\">Сообщество</a><a "
"href=\"donate.html\">Пожертвовать</a>"

#: news.html%2Bhtml.body.nav.div.div.a.img[alt]:145-11
msgid "Download Ceno"
msgstr "Скачать Ceno"

#: news.html%2Bhtml.body.nav.div.div.a.div:146-11
msgctxt "news.html+html.body.nav.div.div.a.div:146-11"
msgid "Download"
msgstr "Скачать"

#: news.html%2Bhtml.body.nav.div.a.img[alt]:149-28
msgid "Ceno logo"
msgstr "Ceno лого"

#: news.html%2Bhtml.body.nav.div.div.label.img[alt]:153-11
msgctxt "news.html+html.body.nav.div.div.label.img[alt]:153-11"
msgid "Select language"
msgstr "Выбрать язык"

#.  Sorted alphabetically by language code. 
#.  TRANSLATORS: Please do not translate any of these. 
#: news.html%2Bhtml.body.nav.div.div.div:159-9
#, fuzzy
msgctxt "news.html+html.body.nav.div.div.div:159-9"
msgid ""
"<a class=\"navbar-item\" lang=\"ar\" hreflang=\"ar\" href=\"../ar/news."
"html\">العربية</a> <a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" "
"href=\"../en/news.html\">English</a> <a class=\"navbar-item\" lang=\"es\" "
"hreflang=\"es\" href=\"../es/news.html\">Español</a> <a class=\"navbar-"
"item\" lang=\"fa\" hreflang=\"fa\" href=\"../fa/news.html\">فارسی</a> <a "
"class=\"navbar-item\" lang=\"fr\" hreflang=\"fr\" href=\"../fr/news."
"html\">Français</a> <a class=\"navbar-item\" lang=\"my\" hreflang=\"my\" "
"href=\"../my/news.html\">မြန်မာစာ</a> <a class=\"navbar-item\" lang=\"pt_BR\" "
"hreflang=\"pt_BR\" href=\"../pt_BR/news.html\">Português</a> <a "
"class=\"navbar-item\" lang=\"ru\" hreflang=\"ru\" href=\"../ru/news."
"html\">Pусский</a> <a class=\"navbar-item\" lang=\"tr\" hreflang=\"tr\" "
"href=\"../tr/news.html\">Türkçe</a> <a class=\"navbar-item\" lang=\"uk\" "
"hreflang=\"uk\" href=\"../uk/news.html\">Українська</a> <a class=\"navbar-"
"item\" lang=\"ur\" hreflang=\"ur\" href=\"../ur/news.html\">اردو</a>"
msgstr ""
"<a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" href=\"../en/news."
"html\">English</a> <a class=\"navbar-item\" lang=\"es\" hreflang=\"es\" "
"href=\"../es/news.html\">Español</a> <a class=\"navbar-item\" lang=\"fa\" "
"hreflang=\"fa\" href=\"../fa/news.html\">فارسی</a> <a class=\"navbar-item\" "
"lang=\"fr\" hreflang=\"fr\" href=\"../fr/news.html\">Français</a> <a "
"class=\"navbar-item\" lang=\"my\" hreflang=\"my\" href=\"../my/news."
"html\">မြန်မာစာ</a> <a class=\"navbar-item\" lang=\"ru\" hreflang=\"ru\" "
"href=\"../ru/news.html\">Pусский</a> <a class=\"navbar-item\" lang=\"uk\" "
"hreflang=\"uk\" href=\"../uk/news.html\">Українська</a>"

#: news.html%2Bhtml.body.main.title:178-54
msgid "Ceno Browser | News and Media"
msgstr "Браузер Ceno | Новости и СМИ"

#: news.html%2Bhtml.body.main.div.h1:180-5
msgid "News and Media"
msgstr "Новости и СМИ"

#: news.html%2Bhtml.body.main.div.p:181-2
msgid ""
"Read the latest updates from the Ceno team, check out past coverage, and "
"find resources for journalists and researchers."
msgstr ""
"Читайте последние обновления от команды Ceno, ознакомьтесь с прошлыми "
"репортажами и найдите ресурсы для журналистов и исследователей."

#: news.html%2Bhtml.body.main.div.section.h2:185-2
msgid "Our News"
msgstr "Наши новости"

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.h4:193-17
msgid "Ceno proves itself in Iran during Internet shutdowns"
msgstr "Ceno проявил себя в Иране во время отключения Интернета"

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.div.p:195-17
msgid "December 15, 2022"
msgstr "15 декабря 2022"

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.h4:206-17
msgid "eQualitie launches Ceno, world's first decentralized p2p mobile browser"
msgstr ""
"eQualitie запускает Ceno, первый в мире децентрализованный p2p мобильный "
"браузер"

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.div.p:208-3
msgid "May 10th, 2022"
msgstr "10 мая 2022 г."

#: news.html%2Bhtml.body.main.div.section.h2:219-1
msgid "Recent Coverage"
msgstr "Недавнее освещение"

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.h4:227-17
msgid "eQualitie presented the Ceno mobile p2p browser in Ukraine"
msgstr "eQualitie представила мобильный p2p браузер Ceno в Украине"

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.div.p:229-17
msgid "May 21st, 2023"
msgstr "21 мая 2023 г."

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.h4:241-17
msgid "WhatsApp Launches a Tool to Fight Internet Censorship"
msgstr "WhatsApp запускает инструмент для борьбы с интернет-цензурой"

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.div.p:243-17
msgid "January 5th, 2023"
msgstr "5 Июль 2023 г."

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.h4:255-17
msgid ""
"A new media project is working to sneak blocked media into Russia and wants "
"to become a hub for press freedom in totalitarian regimes"
msgstr ""
"Новый медиа-проект работает над тем, чтобы пронести заблокированные СМИ в "
"Россию и хочет стать центром свободы прессы в тоталитарных режимах"

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.div.p:257-17
msgid "September 1st, 2022"
msgstr "1 Сентябрь 2021"

#: news.html%2Bhtml.body.main.div.section.div.p:264-9
msgid "More Coverage >>"
msgstr "Недавнее освещение"

#: news.html%2Bhtml.body.main.div.section.h2:270-1
msgid "Events & Presentations"
msgstr "Мероприятия и презентации"

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.h4:279-17
msgid "Ceno: Avoid Internet Shutdowns with Cooperative Web Browsing (Arabic)"
msgstr ""
"Ceno: Избегайте отключений Интернета с помощью объединенного веб-просмотра "
"(арабский)"

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.p:280-17
msgid "November 26th, 2021"
msgstr "26 ноября 2021"

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.h4:291-17
msgid "Ceno: Route Around Censorship with P2P-powered Web Browsing"
msgstr "Ceno: обойти цензуру с помощью веб-просмотра на основе P2P"

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.p:292-17
msgid "November 4th, 2021"
msgstr "4 ноября 2021"

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.h4:303-17
msgid ""
"Ouinet, a cooperative P2P web cache that helps overcome network interference"
msgstr ""
"Ouinet, совместный P2P веб-кеш, который помогает преодолевать сетевые "
"препятствия"

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.a.div.div.p:304-17
msgid "August 2nd, 2018"
msgstr "2 августа 2018"

#: news.html%2Bhtml.body.main.div.section.h2:317-1
msgid "Resources"
msgstr "Ресурсы"

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.h4:324-17
msgid "Fact Sheet"
msgstr "Информационный бюллетень"

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.div:325-17
msgid "Learn"
msgstr "Учиться"

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.h4:336-17
msgid "Case Studies"
msgstr "Примеры из практики"

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.div:337-17
msgid "Read"
msgstr "Читайте"

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.h4:348-17
msgid "Brand & Style Guide"
msgstr "Руководство по бренду и стилю"

#: news.html%2Bhtml.body.main.div.section.div.div.div.div.div.div.div:349-3
msgid "Explore"
msgstr "Исследовать"

#: news.html%2Bhtml.body.main.div.p:360-2
msgid ""
"For press and media inquiries, please contact Jenny Ryan at: press [at] "
"equalitie [dot] org"
msgstr ""
"По вопросам прессы и СМИ, пожалуйста, свяжитесь с Дженни Райан по адресу: "
"press [@] equalitie [точка] org"

#: news.html%2Bhtml.body.footer.div.p:378-9
msgid ""
"Ceno Browser is a project by <a href=\"https://equalit.ie\">eQualit.ie</a> – "
"a not-for-profit Canadian organization that creates decentralized internet "
"services in support of a more equal and equitable network. eQualitie's "
"solutions are open source, battle proven and developed in mind of our "
"values. Everyday, they enable freedom of association for millions of people "
"online."
msgstr ""
"Ceno Browser – это проект <a href=\"https://equalit.ie\">eQualit.ie</a>, "
"канадской некоммерческой организации, которая создает децентрализованные "
"интернет-сервисы для поддержки более равноправной и равноправной сети. . "
"Решения eQualitie имеют открытый исходный код, проверены в боях и "
"разработаны с учетом наших ценностей. Они ежедневно предоставляют свободу "
"объединений в Интернете миллионам людей."

#.  TRANSLATORS: Please do not translate this. 
#: news.html%2Bhtml.body.footer:380-23
#, fuzzy
#| msgid ""
#| "© Censorship.no! 2018<script>new Date().getFullYear()>2018&&document."
#| "write(\"-\"+new Date().getFullYear());</script>"
msgid ""
"© eQualitie 2018<script>new Date().getFullYear()>2018&&document.write(\"-"
"\"+new Date().getFullYear());</script>"
msgstr ""
"© Censorship.no! 2018<script>new Date().getFullYear()>2018&&document."
"write(\"-\"+new Date().getFullYear());</script>"

#, fuzzy
#~ msgctxt "news.html+html.body.nav.div.div.div:155-9"
#~ msgid ""
#~ "<a class=\"navbar-item\" lang=\"ar\" hreflang=\"ar\" href=\"../ar/news."
#~ "html\">العربية</a> <a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" "
#~ "href=\"../en/news.html\">English</a> <a class=\"navbar-item\" lang=\"es\" "
#~ "hreflang=\"es\" href=\"../es/news.html\">Español</a> <a class=\"navbar-"
#~ "item\" lang=\"fa\" hreflang=\"fa\" href=\"../fa/news.html\">فارسی</a> <a "
#~ "class=\"navbar-item\" lang=\"fr\" hreflang=\"fr\" href=\"../fr/news."
#~ "html\">Français</a> <a class=\"navbar-item\" lang=\"my\" hreflang=\"my\" "
#~ "href=\"../my/news.html\">မြန်မာစာ</a> <a class=\"navbar-item\" "
#~ "lang=\"pt_BR\" hreflang=\"pt_BR\" href=\"../pt_BR/news.html\">Português</"
#~ "a> <a class=\"navbar-item\" lang=\"ru\" hreflang=\"ru\" href=\"../ru/news."
#~ "html\">Pусский</a> <a class=\"navbar-item\" lang=\"tr\" hreflang=\"tr\" "
#~ "href=\"../tr/news.html\">Türkçe</a> <a class=\"navbar-item\" lang=\"uk\" "
#~ "hreflang=\"uk\" href=\"../uk/news.html\">Українська</a> <a class=\"navbar-"
#~ "item\" lang=\"ur\" hreflang=\"ur\" href=\"../ur/news.html\">اردو</a>"
#~ msgstr ""
#~ "<a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" href=\"../en/news."
#~ "html\">English</a> <a class=\"navbar-item\" lang=\"es\" hreflang=\"es\" "
#~ "href=\"../es/news.html\">Español</a> <a class=\"navbar-item\" lang=\"fa\" "
#~ "hreflang=\"fa\" href=\"../fa/news.html\">فارسی</a> <a class=\"navbar-"
#~ "item\" lang=\"fr\" hreflang=\"fr\" href=\"../fr/news.html\">Français</a> "
#~ "<a class=\"navbar-item\" lang=\"my\" hreflang=\"my\" href=\"../my/news."
#~ "html\">မြန်မာစာ</a> <a class=\"navbar-item\" lang=\"ru\" hreflang=\"ru\" "
#~ "href=\"../ru/news.html\">Pусский</a> <a class=\"navbar-item\" lang=\"uk\" "
#~ "hreflang=\"uk\" href=\"../uk/news.html\">Українська</a>"

#~ msgid "About"
#~ msgstr "О проекте"

#~ msgid "Support"
#~ msgstr "Поддержка"

#~ msgid "News"
#~ msgstr "Новости"

#~ msgid "Community"
#~ msgstr "Коммьюнити"

#~ msgid "Donate"
#~ msgstr "Пожертвовать"

#~ msgid "Get Ceno"
#~ msgstr "Установить Ceno"

#~ msgid ""
#~ "Ceno Browser is a project by <a href=\"https://equalit.ie\">eQualit.ie</"
#~ "a> – a not-for-profit Canadian company developing open and reusable "
#~ "systems with a focus on privacy, online security and freedom of "
#~ "association. Technology solutions and innovations are driven by our <a "
#~ "href=\"https://equalit.ie/en/values/\">values</a> that guide us to "
#~ "protect the rights and aspirations of everyone online."
#~ msgstr ""
#~ "Ceno Browser - это проект, разработанный <a href=\"https://equalit."
#~ "ie\">eQualit.ie</a>- некоммерческая канадская организация, "
#~ "разрабатывающая открытые и многоразовые системы с акцентом на "
#~ "конфиденциальность, онлайн-безопасность и свободу убеждений. В основе "
#~ "технологических решений и инноваций лежат наши<a href=\"https://equalit."
#~ "ie/en/values/\">ценности</a>, которые определяют наши действия по защите "
#~ "прав и стремлений каждого человека в Интернете."

#~ msgid ""
#~ "The Ceno browser is gaining popularity on the Internet - everything opens "
#~ "with it without a VPN"
#~ msgstr ""
#~ "В интернете набирает популярность браузер Ceno – с ним все открывается "
#~ "без VPN"

#~ msgid "August 12th, 2022"
#~ msgstr "12 Август 2022 г."

#, fuzzy
#~| msgctxt "news.html+html.body.main.title:137-1"
#~| msgid "CENO Browser | News and Media"
#~ msgid "CENO Browser | News and Media"
#~ msgstr "Браузер CENO | Новости и СМИ"

#, fuzzy
#~ msgid ""
#~ "<a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" href=\"../en/news."
#~ "html\">English</a> <a class=\"navbar-item\" lang=\"es\" hreflang=\"es\" "
#~ "href=\"../es/news.html\">Español</a> <a class=\"navbar-item\" lang=\"fa\" "
#~ "hreflang=\"fa\" href=\"../fa/news.html\">فارسی</a> <a class=\"navbar-"
#~ "item\" lang=\"fr\" hreflang=\"fr\" href=\"../fr/news.html\">Français</a> "
#~ "<a class=\"navbar-item\" lang=\"my\" hreflang=\"my\" href=\"../my/news."
#~ "html\">မြန်မာစာ</a> <a class=\"navbar-item\" lang=\"my\" hreflang=\"my\" "
#~ "href=\"../pt_BR/news.html\">Português</a> <a class=\"navbar-item\" "
#~ "lang=\"ru\" hreflang=\"ru\" href=\"../ru/news.html\">Pусский</a> <a "
#~ "class=\"navbar-item\" lang=\"tr\" hreflang=\"tr\" href=\"../tr/news."
#~ "html\">Türkçe</a> <a class=\"navbar-item\" lang=\"uk\" hreflang=\"uk\" "
#~ "href=\"../uk/news.html\">Українська</a> <a class=\"navbar-item\" "
#~ "lang=\"ur\" hreflang=\"ur\" href=\"../ur/news.html\">اردو</a>"
#~ msgstr ""
#~ "<a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" href=\"../en/news."
#~ "html\">English</a> <a class=\"navbar-item\" lang=\"es\" hreflang=\"es\" "
#~ "href=\"../es/news.html\">Español</a> <a class=\"navbar-item\" lang=\"fa\" "
#~ "hreflang=\"fa\" href=\"../fa/news.html\">فارسی</a> <a class=\"navbar-"
#~ "item\" lang=\"fr\" hreflang=\"fr\" href=\"../fr/news.html\">Français</a> "
#~ "<a class=\"navbar-item\" lang=\"my\" hreflang=\"my\" href=\"../my/news."
#~ "html\">မြန်မာစာ</a> <a class=\"navbar-item\" lang=\"ru\" hreflang=\"ru\" "
#~ "href=\"../ru/news.html\">Pусский</a> <a class=\"navbar-item\" lang=\"uk\" "
#~ "hreflang=\"uk\" href=\"../uk/news.html\">Українська</a>"

#~ msgctxt "news.html+html.body.main.title:138-1"
#~ msgid "CENO Browser | News and Media"
#~ msgstr "Браузер CENO | Новости и СМИ"

#~ msgctxt "news.html+html.body.main.title:136-1"
#~ msgid "CENO Browser | News and Media"
#~ msgstr "Браузер CENO | Новости и СМИ"

#~ msgctxt "news.html+html.body.main.title:132-1"
#~ msgid "CENO Browser | News and Media"
#~ msgstr "Браузер CENO | Новости и СМИ"

#~ msgctxt "news.html+html.body.main.title:129-1"
#~ msgid "CENO Browser | News and Media"
#~ msgstr "Браузер CENO | Новости и СМИ"

#~ msgctxt "news.html+html.body.main.title:128-1"
#~ msgid "CENO Browser | News and Media"
#~ msgstr "Браузер CENO | Новости и СМИ"

#~ msgid ""
#~ "How a Canadian organization is creating a digital security emergency "
#~ "service for Ukrainians"
#~ msgstr ""
#~ "Как канадская организация создает экстренную службу цифровой безопасности "
#~ "для украинцев"

#~ msgid ""
#~ "Resources to Support Human Rights Online During the Invasion of Ukraine"
#~ msgstr ""
#~ "Ресурсы для Поддержки Прав Человека в Интернете во время Вторжения в "
#~ "Украину"

#~ msgid "April 12th, 2022"
#~ msgstr "12 апреля 2022 г."

#~ msgid ""
#~ "If they turn off the Internet: how to stay connected in case of an "
#~ "emergency"
#~ msgstr ""
#~ "Если они выключат интернет: как оставаться на связи в случае чрезвычайной "
#~ "ситуации"

#~ msgid "February 24th, 2022"
#~ msgstr "24 февраля 2022 г."

#~ msgid ""
#~ "How a fight over anti-censorship funding became a symbol of Trump "
#~ "administration turmoil (PBS NewsHour)"
#~ msgstr ""
#~ "Как борьба за антицензурное финансирование стала символом беспорядков в "
#~ "администрации Трампа (PBS NewsHour)"

#~ msgid "February 12, 2021"
#~ msgstr "12 февраля 2021 г."

#~ msgctxt "news.html+html.body.main.title:127-1"
#~ msgid "CENO Browser | News and Media"
#~ msgstr "Браузер CENO | Новости и СМИ"

#~ msgid ""
#~ "<a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" href=\"../en/news."
#~ "html\">English</a> <a class=\"navbar-item\" lang=\"es\" hreflang=\"es\" "
#~ "href=\"../es/news.html\">Español</a> <a class=\"navbar-item\" lang=\"fa\" "
#~ "hreflang=\"fa\" href=\"../fa/news.html\">فارسی</a> <a class=\"navbar-"
#~ "item\" lang=\"my\" hreflang=\"my\" href=\"../my/news.html\">မြန်မာစာ</a> "
#~ "<a class=\"navbar-item\" lang=\"ru\" hreflang=\"ru\" href=\"../ru/news."
#~ "html\">Pусский</a> <a class=\"navbar-item\" lang=\"uk\" hreflang=\"uk\" "
#~ "href=\"../uk/news.html\">Українська</a>"
#~ msgstr ""
#~ "<a class=\"navbar-item\" lang=\"en\" hreflang=\"en\" href=\"../en/news."
#~ "html\">English</a> <a class=\"navbar-item\" lang=\"es\" hreflang=\"es\" "
#~ "href=\"../es/news.html\">Español</a> <a class=\"navbar-item\" lang=\"fa\" "
#~ "hreflang=\"fa\" href=\"../fa/news.html\">فارسی</a> <a class=\"navbar-"
#~ "item\" lang=\"my\" hreflang=\"my\" href=\"../my/news.html\">မြန်မာစာ</a> "
#~ "<a class=\"navbar-item\" lang=\"ru\" hreflang=\"ru\" href=\"../ru/news."
#~ "html\">Pусский</a> <a class=\"navbar-item\" lang=\"uk\" hreflang=\"uk\" "
#~ "href=\"../uk/news.html\">Українська</a>"
